import base64
import json
import os

from vault.repository import RepositoryError, RepositoryNotFoundError, S3Repository

# Get the bucket from the environment set in serverless.yml
bucket = os.environ.get("REPOSITORY_BUCKET")
# Create a repository wrapping the bucket
repository = S3Repository(bucket)


def create(event, context):
    key = repository.unique_key() + ".pdf"

    if event["isBase64Encoded"]:
        outer_b64 = event["body"]
        bytes = base64.b64decode(outer_b64)
        string = bytes.decode("utf-8")
    else:
        string = event["body"]

    request = json.loads(string)
    b64 = request["document"]
    repository.create_b64(key, b64)

    response = {"statusCode": 200, "body": json.dumps({"id": key})}

    return response


def retrieve(event, context):
    try:
        key = event["pathParameters"]["id"]
        b64 = repository.retrieve_b64(key)

        return {
            "statusCode": 200,
            "body": b64,
            "headers": {"Content-Type": "application/pdf"},
            "isBase64Encoded": True,
        }
    except RepositoryNotFoundError:
        return {
            "statusCode": 404,
        }


def update(event, context):
    try:
        if event["isBase64Encoded"]:
            outer_b64 = event["body"]
            bytes = base64.b64decode(outer_b64)
            string = bytes.decode("utf-8")
        else:
            string = event["body"]

        request = json.loads(string)
        b64 = request["document"]
        key = event["pathParameters"]["id"]
        repository.update_b64(key, b64)

        return {"statusCode": 204}
    except RepositoryNotFoundError:
        return {"statuCode": 404}


def delete(event, context):
    key = event["pathParameters"]["id"]
    repository.delete(key)

    return {"statusCode": 204}
