import base64
import boto3
import uuid

s3 = boto3.client("s3", region_name="eu-west-2")


class S3Repository:
    s3 = boto3.client("s3", region_name="eu-west-2")

    def __init__(self, bucket_name, client=s3) -> None:
        self.bucket_name = bucket_name
        self.client = client

    def create(self, key: str, body: bytes) -> None:
        self.client.put_object(Bucket=self.bucket_name, Key=key, Body=body)

    def create_b64(self, key: str, b64: str) -> None:
        bytes = base64.b64decode(b64)
        self.create(key, bytes)

    def retrieve(self, key: str) -> bytes:
        try:
            response = self.client.get_object(Bucket=self.bucket_name, Key=key)
            return response["Body"].read()
        except s3.exceptions.NoSuchKey:
            raise RepositoryNotFoundError(f"File with key {key} was not found.")

    def retrieve_b64(self, key: str) -> str:
        bytes = self.retrieve(key)
        return base64.b64encode(bytes).decode("utf-8")

    def update(self, key: str, body: bytes) -> None:
        # If the object doesn't exist then throw an exception
        try:
            self.client.get_object(Bucket=self.bucket_name, Key=key)
        except s3.exceptions.NoSuchKey:
            print(f"File with key {key} was not found.")
            raise RepositoryNotFoundError(f"File with key {key} was not found.")

        self.client.put_object(Bucket=self.bucket_name, Key=key, Body=body)

    def update_b64(self, key: str, b64: str) -> None:
        bytes = base64.b64decode(b64)
        self.update(key, bytes)

    def delete(self, key: str) -> None:
        # If the object doesn't exist then throw an exception
        try:
            self.client.get_object(Bucket=self.bucket_name, Key=key)
        except s3.exceptions.NoSuchKey:
            raise RepositoryNotFoundError(f"File with key {key} was not found.")

        self.client.delete_object(Bucket=self.bucket_name, Key=key)

    def unique_key(self) -> str:
        return str(uuid.uuid4())


class RepositoryError(Exception):
    """Base class for exceptions in repository module."""

    pass


class RepositoryNotFoundError(RepositoryError):
    def __init__(self, message):
        self.message = message
