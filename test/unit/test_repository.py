import base64
import unittest
from unittest.mock import Mock
from uuid import UUID
from vault.repository import S3Repository


class TestS3Repository(unittest.TestCase):
    def setUp(self) -> None:
        self.bucket_name = "some_bucket"
        self.s3 = Mock()
        self.repository = S3Repository(self.bucket_name, client=self.s3)

    def test_bucket_name(self):
        self.assertEqual(self.repository.bucket_name, self.bucket_name)

    def test_create(self):
        self.repository.create("some_key", b"some_bytes")
        self.s3.put_object.assert_called_once_with(
            Bucket="some_bucket", Key="some_key", Body=b"some_bytes"
        )

    def test_createb_64(self):
        body = base64.b64encode(b"some_bytes").decode("utf-8")
        self.repository.create_b64("some_key", body)
        self.s3.put_object.assert_called_once_with(
            Bucket="some_bucket", Key="some_key", Body=b"some_bytes"
        )

    def test_retrieve(self):
        self.s3.get_object.return_value = {
            "Body": StreamingBody("This is the document content")
        }
        body = self.repository.retrieve("some_key")
        self.s3.get_object.assert_called_once_with(Bucket="some_bucket", Key="some_key")
        self.assertTrue(type(body) is bytes)
        self.assertEqual(b"This is the document content", body)

    def test_retrieve_b64(self):
        self.s3.get_object.return_value = {
            "Body": StreamingBody("This is the document content")
        }
        body = self.repository.retrieve_b64("some_key")
        self.s3.get_object.assert_called_once_with(Bucket="some_bucket", Key="some_key")
        self.assertTrue(type(body) is str)
        self.assertEqual(base64.b64encode(b"This is the document content").decode("utf-8"), body)

    def test_update(self):
        self.repository.update("some_key", b"some_bytes")
        self.s3.put_object.assert_called_once_with(
            Bucket="some_bucket", Key="some_key", Body=b"some_bytes"
        )

    def test_update_b64(self):
        body = base64.b64encode(b"some_bytes").decode("utf-8")
        self.repository.update_b64("some_key", body)
        self.s3.put_object_assert_called_once_with(
            Bucket="some_bucket", Key="some_key", Body=b"some_bytes"
        )


    def test_delete(self):
        self.repository.delete("some_key")
        self.s3.delete_object.assert_called_once_with(
            Bucket="some_bucket", Key="some_key"
        )

    def test_unique_key(self):
        key = self.repository.unique_key()
        try:
            obj = UUID(key, version=4)
            self.assertEqual(obj.version, 4)
        except ValueError:
            self.assertTrue(False)


class StreamingBody:
    def __init__(self, string: str):
        self.string = string

    def read(self) -> bytes:
        return self.string.encode("utf-8")
