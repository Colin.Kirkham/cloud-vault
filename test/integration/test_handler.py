import json
import os
import requests
import unittest

from vault.repository import S3Repository

BASE_URL = "https://wbrx1qbnwh.execute-api.eu-west-2.amazonaws.com"
REPOSITORY = "vault-cloud-repository"


class TestHandler(unittest.TestCase):
    def tearDown(self) -> None:
        repository = CleanableS3Repository(REPOSITORY)
        repository.clean()

    def test_create(self):
        # Upload a known document
        res = upload_document("data/document-1.json")
        # Read the id from the response
        obj = json.loads(res.content.decode("utf-8"))
        # Ensure the id we get back is in a suitable form
        self.assertIsNotNone(obj)
        self.assertIsNotNone(obj["id"])
        self.assertEqual(type(obj["id"]), str)
        self.assertTrue(obj["id"].endswith(".pdf"))

    def test_retrieve(self):
        # Upload a known document
        res = upload_document("data/document-1.json")
        # Read the id from the response
        obj = json.loads(res.content.decode("utf-8"))
        id = obj["id"]
        # Get the uploaded document
        res = requests.get(
            BASE_URL + "/vault/v1/document/" + id,
            headers={"Content-Type": "application/pdf"},
        )
        # Save it to a file
        with open("data/temp.pdf", "wb") as f:
            f.write(res.content)
        # Compare the two file sizes and ensure they are the same
        self.assertEqual(
            os.path.getsize("data/temp.pdf"), os.path.getsize("data/document-1.pdf")
        )
        # Clean up the local file
        os.remove("data/temp.pdf")

    def test_update(self):
        # Upload a known document
        res = upload_document("data/document-1.json")
        # Read the id from the response
        obj = json.loads(res.content.decode("utf-8"))
        id = obj["id"]
        # Open a second, known document
        with open("data/document-2.json", "r") as f:
            data = f.read()
        # Upload this document with the same id as the original
        res = requests.put(
            BASE_URL + "/vault/v1/document/" + id,
            data=data,
            headers={"Content-Type": "application/json"},
        )
        self.assertEqual(res.status_code, 204)
        # Get the document with our id again
        res = requests.get(
            BASE_URL + "/vault/v1/document/" + id,
            headers={"Content-Type": "application/pdf"},
        )
        # Save it to a file
        with open("data/temp.pdf", "wb") as f:
            f.write(res.content)
        # Compare the two file sizes and ensure they are the same
        self.assertEqual(
            os.path.getsize("data/temp.pdf"), os.path.getsize("data/document-2.pdf")
        )
        # Clean up the local file
        os.remove("data/temp.pdf")

    def test_delete(self):
        # Upload a known document
        res = upload_document("data/document-1.json")
        # Read the id from the response
        obj = json.loads(res.content.decode("utf-8"))
        id = obj["id"]
        # Delete the document
        res = requests.delete(BASE_URL + "/vault/v1/document/" + id)
        self.assertEqual(res.status_code, 204)
        # Try and get the document
        res = requests.get(BASE_URL + "/vault/v1/document/" + id)
        self.assertEqual(res.status_code, 404)


def upload_document(path: str) -> requests.Response:
    with open(path, "r") as f:
        data = f.read()

    return requests.post(
        BASE_URL + "/vault/v1/document",
        data=data,
        headers={"Content-Type": "application/json"},
    )


class CleanableS3Repository(S3Repository):
    def clean(self):
        response = self.client.list_objects_v2(Bucket=self.bucket_name)
        keys = [item["Key"] for item in response["Contents"]]
        for key in keys:
            response = self.client.delete_object(Bucket=self.bucket_name, Key=key)
