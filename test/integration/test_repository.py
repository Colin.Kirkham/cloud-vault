import boto3
import unittest
from vault.repository import S3Repository, RepositoryNotFoundError

s3 = boto3.client("s3")
bucket_name = "cloud-vault-test-bucket"


class TestRepository(unittest.TestCase):
    def setUp(self) -> None:
        self.repository = S3Repository(bucket_name)

    def test_create(self):
        # Test
        key = "some_key"
        content = b"some_body"
        self.repository.create(key, content)
        response = s3.get_object(Bucket=bucket_name, Key=key)
        self.assertEqual(content, response["Body"].read())
        # Tidy up
        s3.delete_object(Bucket=bucket_name, Key=key)

    def test_retrieve(self):
        # Test
        key = "retrieve_key"
        content = b"content for retrieval"
        s3.put_object(Bucket=bucket_name, Key=key, Body=content)
        response = self.repository.retrieve(key=key)
        self.assertEqual(content, response)
        # Tidy up
        s3.delete_object(Bucket=bucket_name, Key=key)

    def test_retrieve_missing_key(self):
        # Test
        key = "retrieve_missing_key"
        self.assertRaises(RepositoryNotFoundError, self.repository.retrieve, key)

    def test_update(self):
        # Test
        key = "update_key"
        old_content = b"old content"
        new_content = b"new content"
        s3.put_object(Bucket=bucket_name, Key=key, Body=old_content)
        self.repository.update(key, new_content)
        response = s3.get_object(Bucket=bucket_name, Key=key)
        self.assertEqual(new_content, response["Body"].read())
        # Tidy up
        s3.delete_object(Bucket=bucket_name, Key=key)

    def test_update_missing_key(self):
        # Test
        key = "update_missing_key"
        body = b"new content"
        self.assertRaises(RepositoryNotFoundError, self.repository.update, key, body)

    def test_delete(self):
        # Test
        key = "delete_key"
        body = b"some content"
        s3.put_object(Bucket=bucket_name, Key=key, Body=body)
        self.repository.delete(key)
        response = s3.list_objects_v2(Bucket=bucket_name)
        files = response.get("Contents", [])
        self.assertEqual(len(files), 0)

    def test_delete_missing_key(self):
        # Test
        key = "delete_missing_key"
        body = b"new content"
        self.assertRaises(RepositoryNotFoundError, self.repository.delete, key)
        